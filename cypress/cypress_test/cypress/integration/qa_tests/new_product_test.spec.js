
describe('This test try to create a new product using the API and also try to verify if the product was success created using the UI and also try to delete the product created via API', () => {

    it('add a new product via API', () => {
       
      cy.fixture('product').then(product => {
        cy
            .request(
                {
                    method: 'POST',
                    url: '/wp-json/wc/v3/products' ,
                    body: product, 
                    auth: {
                        'user': Cypress.env('qa_user'),
                        'pass': Cypress.env('qa_password')
                    }
                })
            .then((response) => {
            // response.body is automatically serialized into JSON
            cy.writeFile('cypress/fixtures/product_created.json', response.body)
            expect(response.body).to.have.property('name', product.name)  
            })
        })
    })

    it('navigate to the created product via UI', () => {
      
      cy.fixture('product').then(product => {
        cy.visit('/product/' + product.name)
        cy.title().should('include', product.name + ' – QA Playground')
        cy.get('.price span').should('be.visible') 

        cy.get('input[id^=quantity_]').type('{selectall}{del}7{enter}')
        cy.get('input[id^=quantity_]').should('have.value', '7')

        cy.get('[name="add-to-cart"]').click()
        cy.visit('/cart/')
        cy.get('[class="product-name"] a').should('have.text', product.name)
        cy.get('.product-price span').contains(product.regular_price)
        cy.get('input[id^=quantity_]').should('have.value', '7')
      })
         
    })

  
    it('Delete the product via API', () => {
      
      cy.fixture('product_created').then(product => {
        cy
          .request(
              {
                  method: 'DELETE',
                  url: '/wp-json/wc/v3/products/' + product.id,
                  auth: {
                    'user': Cypress.env('qa_user'),
                    'pass': Cypress.env('qa_password')
                }
              })
          .then((response) => {
          expect(response.body).to.have.property('name', product.name) // true
          })
      })
    })


 })