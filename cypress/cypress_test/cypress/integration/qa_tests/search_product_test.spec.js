describe('this test try to verify if a user can user the search functionality', () => {


    it('navigate to the homepage', () => {
        cy.visit('')
        cy.title().should('include', 'QA Playground – Just another WordPress site')
        cy.get('#woocommerce-product-search-field-0').should('be.visible')

    });

    it('Type in the search input', () => {
        cy.get('#woocommerce-product-search-field-0').type('Hoodie')

    });

    it('Press Enter in the search input and validate the products are displayed', () => {
        cy.get('#woocommerce-product-search-field-0').type('{enter}')
        cy.get('[class="products columns-3"] li').should(($li) => {
            expect($li).to.have.length(4)
        })
    });

    it('Click on “Hoodie with Pocket”', () => {
        cy.get('h2:contains("Hoodie with Pocket")').trigger('mouseover')
        cy.get('h2:contains("Hoodie with Pocket")').click({ force: true })
        cy.title().should('include', 'Hoodie with Pocket – QA Playground')

    });

});