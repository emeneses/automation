describe('"Book Search', () => {

  it('Search for book title', () => {
      cy.visit('/index')
      cy.title().should('include', 'Library Page')
      cy.get('#search_bar').clear()
      cy.get('#search_bar').type('bookTitle')
      cy.get('.title').should('include', 'Book: JavaScript For Testers')
  })

  it('Search for book author', () => {
      cy.visit('/index')
      cy.title().should('include', 'Library Page')
      cy.get('#search_bar').clear()
      cy.get('#search_bar').type('bookTitle')
      cy.get('.author').should('include', 'Author: Stephen Hawking')
  })


})