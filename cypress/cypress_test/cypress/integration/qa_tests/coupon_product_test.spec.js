
describe('This test try to apply a discount in a product', () => {

    it('add a new product via API', () => {
       
        cy.fixture('product').then(product => {
          cy
              .request(
                  {
                      method: 'POST',
                      url: '/wp-json/wc/v3/products' ,
                      body: product, 
                      auth: {
                        'user': Cypress.env('qa_user'),
                        'pass': Cypress.env('qa_password')
                    }
                  })
              .then((response) => {
              // response.body is automatically serialized into JSON
              cy.writeFile('cypress/fixtures/product_created.json', response.body)
              expect(response.body).to.have.property('name', product.name)  
              })
          })
      })

      it('add a new coupon via API', () => {
        cy.fixture('coupon').then(coupon => {
        cy
          .request(
              {
                  method: 'POST',
                  url: '/wp-json/wc/v3/coupons' ,
                  body: coupon, 
                  auth: {
                    'user': Cypress.env('qa_user'),
                    'pass': Cypress.env('qa_password')
                }
              })
          .then((response) => {
            cy.writeFile('cypress/fixtures/coupon_created.json', response.body)
            expect(response.body).to.have.property('code', coupon.code)
          })
        })
      })
    
      it('Apply coupon to the product via UI', () => {
        cy.fixture('product').then(product => {
            cy.visit('/product/' + product.name)
            cy.title().should('include', product.name + ' – QA Playground')
            cy.get('[name="add-to-cart"]').click()
            cy.get('#site-header-cart').click()
            cy.get('[class="product-name"] a').should('have.text', product.name)
            cy.get('[name="coupon_code"]').type('100off_demo')
            cy.get('[name="apply_coupon"]').click()
            cy.get('div:contains("Coupon code applied successfully.")').should('be.visible') 
        })

    })


    it('Delete the product via API', () => {
      
        cy.fixture('product_created').then(product => {
          cy
            .request(
                {
                    method: 'DELETE',
                    url: '/wp-json/wc/v3/products/' + product.id,
                    auth: {
                        'user': Cypress.env('qa_user'),
                        'pass': Cypress.env('qa_password')
                    }
                })
            .then((response) => {
            expect(response.body).to.have.property('name', product.name) // true
            })
        })
      })

      it('Delete coupon via API', () => {
        cy.fixture('coupon_created').then(coupon => {

            cy
                .request(
                    {
                        method: 'DELETE',
                        url: '/wp-json/wc/v3/coupons/' + coupon.id + '?force=true', 
                        auth: {
                            'user': Cypress.env('qa_user'),
                            'pass': Cypress.env('qa_password')
                        }
                    })
                .then((response) => {
                expect(response.body).to.have.property('code', coupon.code)
                })
            
        })
      })


})

